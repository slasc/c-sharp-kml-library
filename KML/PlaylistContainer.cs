﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.google.com/kml/ext/2.2")]
    public class PlaylistContainer
    {
        [System.Xml.Serialization.XmlElementAttribute("FlyTo")]
        public FlyTo Flyto;

        [System.Xml.Serialization.XmlElementAttribute("Wait")]
        public Wait Wait;
    }
}

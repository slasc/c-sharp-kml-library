﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    public class Update
    {
        [System.Xml.Serialization.XmlElementAttribute("targetHref", Namespace = "http://www.opengis.net/kml/2.2")]
        public string targetHref;

        [System.Xml.Serialization.XmlElementAttribute("Change")]
        public Change Change = null;
    }
}

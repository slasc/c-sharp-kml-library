﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    public class Change
    {
        [System.Xml.Serialization.XmlElementAttribute("LineStyle")]
        public LineStyle LineStyle;

        [System.Xml.Serialization.XmlElementAttribute("PolyStyle")]
        public PolyStyle PolyStyle;

        [System.Xml.Serialization.XmlElementAttribute("LabelStyle")]
        public LabelStyle LabelStyle;

        [System.Xml.Serialization.XmlElementAttribute("IconStyle")]
        public IconStyle IconStyle;

        [System.Xml.Serialization.XmlElementAttribute("BalloonStyle")]
        public BalloonStyle BalloonStyle;

        [System.Xml.Serialization.XmlElementAttribute("Placemark")]
        public Placemark Placemark;


    }
}

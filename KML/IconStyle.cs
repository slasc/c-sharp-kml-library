﻿using System;

namespace KML
{
	public class IconStyle
	{
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "ID")]
        public string id;

        [System.Xml.Serialization.XmlAttributeAttribute(AttributeName = "targetId")]
        public string targetId;

        [System.Xml.Serialization.XmlElementAttribute("Icon")]
		public Icon Icon;

		[System.Xml.Serialization.XmlElementAttribute("color")]
		public string color;

		[System.Xml.Serialization.XmlElementAttribute("colorMode")]
		public string colorMode;

		[System.Xml.Serialization.XmlElementAttribute("scale")]
		public string scale;

		[System.Xml.Serialization.XmlElementAttribute("heading")]
		public string heading;

        [System.Xml.Serialization.XmlElementAttribute("hotSpot")]
        public hotSpot hotSpot;

		public IconStyle ()
		{
		}
	}
}


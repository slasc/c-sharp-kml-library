﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    public class BalloonStyle
    {
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "ID")]
        public string id;

        [System.Xml.Serialization.XmlElementAttribute("name")]
        public string name;

        [System.Xml.Serialization.XmlElementAttribute("description")]
        public string description;

        [System.Xml.Serialization.XmlElementAttribute("styleUrl")]
        public string styleUrl;

        [System.Xml.Serialization.XmlElementAttribute("bgColor")]
        public string bgColor;

        [System.Xml.Serialization.XmlElementAttribute("textColor")]
        public string textColor;

        [System.Xml.Serialization.XmlElementAttribute("text")]
        public CDATA text = null;

        [System.Xml.Serialization.XmlElementAttribute("displayMode")]
        public string displayMode;
    }
}

﻿using System;

namespace KML
{
	public class LabelStyle
	{
		[System.Xml.Serialization.XmlElementAttribute("color")]
		public string color;

		[System.Xml.Serialization.XmlElementAttribute("colorMode")]
		public string colorMode;

		[System.Xml.Serialization.XmlElementAttribute("scale")]
		public string scale;

		public LabelStyle ()
		{
		}
	}
}


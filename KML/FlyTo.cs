﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    public class FlyTo : Playlist
    {
        [System.Xml.Serialization.XmlElementAttribute("duration")]
        public double duration = 0.0;

        [System.Xml.Serialization.XmlElementAttribute("flyToMode")]
        public string flyToMode = string.Empty;

        [System.Xml.Serialization.XmlElementAttribute("Camera", Namespace = "http://www.opengis.net/kml/2.2")]
        public Camera Camera;

        [System.Xml.Serialization.XmlElementAttribute("LookAt", Namespace = "http://www.opengis.net/kml/2.2")]
        public LookAt LookAt;
    }
}

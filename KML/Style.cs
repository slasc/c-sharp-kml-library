﻿using System;

namespace KML
{
	public class Style
	{
		[System.Xml.Serialization.XmlAttributeAttribute(DataType="ID")]
		public string id;

		[System.Xml.Serialization.XmlElementAttribute("LineStyle")]
		public LineStyle LineStyle;

		[System.Xml.Serialization.XmlElementAttribute("PolyStyle")]
		public PolyStyle PolyStyle;

		[System.Xml.Serialization.XmlElementAttribute("LabelStyle")]
		public LabelStyle LabelStyle;

		[System.Xml.Serialization.XmlElementAttribute("IconStyle")]
		public IconStyle IconStyle;

		[System.Xml.Serialization.XmlElementAttribute("BalloonStyle")]
		public BalloonStyle BalloonStyle;

		public Style ()
		{
		}
	}
}


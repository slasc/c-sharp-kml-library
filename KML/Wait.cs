﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    public class Wait : Playlist
    {
        [System.Xml.Serialization.XmlElementAttribute("duration")]
        public double duration = 0.0;
    }
}

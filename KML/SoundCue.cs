﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    public class SoundCue : Playlist
    {
        [System.Xml.Serialization.XmlElementAttribute("href", Namespace = "http://www.opengis.net/kml/2.2")]
        public string href;

        [System.Xml.Serialization.XmlElementAttribute("delayedStart")]
        public double delayedStart = 0.0;
    }
}

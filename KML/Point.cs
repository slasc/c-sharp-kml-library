﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    public class Point
    {
        [System.Xml.Serialization.XmlElementAttribute("coordinates")]
        public string coordinates = string.Empty;

        [System.Xml.Serialization.XmlElementAttribute("altitudeMode")]
        public string altitudeMode;

        public void SetPoint(double latitude, double longitude)
        {
			this.coordinates = string.Format("{0},{1}", longitude, latitude);

        }

        public void SetPoint(double latitude, double longitude, double altitude)
        {
			this.coordinates = string.Format("{0},{1},{2}", longitude, latitude, altitude);

        }
    }
}

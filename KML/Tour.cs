﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KML
{
    [XmlRoot("gx", Namespace = "http://www.google.com/kml/ext/2.2")]
    public class Tour
    {
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "ID", Namespace = "http://www.opengis.net/kml/2.2")]
        public string id;

        [System.Xml.Serialization.XmlElementAttribute("name", Namespace = "http://www.opengis.net/kml/2.2")]
        public string name;

        [System.Xml.Serialization.XmlElementAttribute("description", Namespace = "http://www.opengis.net/kml/2.2")]
        public string description;


        //[System.Xml.Serialization.XmlElementAttribute("Playlist")]
        //        public Playlist[] Playlist;
        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute( "Playlist")]
        //[XmlChoiceIdentifier("FlyTo")]
        //[XmlChoiceIdentifier("Wait")]
        [XmlArray("Playlist")]
        [XmlArrayItem("FlyTo", typeof(FlyTo))]
        [XmlArrayItem("Wait", typeof(Wait))]
        [XmlArrayItem("SoundCue", typeof(SoundCue))]
        [XmlArrayItem("AnimatedUpdate", typeof(AnimatedUpdate))]
        public Playlist[] Playlist;


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KML
{
    [XmlType( AnonymousType = true, Namespace = "http://www.google.com/kml/ext/2.2")]
    //[System.Xml.Serialization.XmlInclude(typeof(Wait))]
    //[System.Xml.Serialization.XmlInclude(typeof(FlyTo))]
    public abstract class Playlist
    {
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "ID", Namespace = "http://www.opengis.net/kml/2.2")]
        public string id;

        [System.Xml.Serialization.XmlElementAttribute("name", Namespace = "http://www.opengis.net/kml/2.2")]
        public string name;

        [System.Xml.Serialization.XmlElementAttribute("description", Namespace = "http://www.opengis.net/kml/2.2")]
        public string description;

    }
}

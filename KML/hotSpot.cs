﻿using System;

namespace KML
{
    public class hotSpot
    {
        //System.Xml.Serialization.XmlAttributeAttribute(DataType="ID")]
        [System.Xml.Serialization.XmlAttributeAttribute("x")]
        public string x;

        [System.Xml.Serialization.XmlAttributeAttribute("y")]
        public string y;

        [System.Xml.Serialization.XmlAttributeAttribute("xunits")]
        public string xunits;

        [System.Xml.Serialization.XmlAttributeAttribute("yunits")]
        public string yunits;


        public hotSpot()
        {
        }
    }
}


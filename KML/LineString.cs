﻿using System;

namespace KML
{
	public class LineString
	{
		[System.Xml.Serialization.XmlElementAttribute("extrude")]
		public string extrude;

		[System.Xml.Serialization.XmlElementAttribute("tessellate")]
		public string tessellate;

		[System.Xml.Serialization.XmlElementAttribute("altitude")]
		public string altitude;

		[System.Xml.Serialization.XmlElementAttribute("altitudeMode")]
		public string altitudeMode;

		[System.Xml.Serialization.XmlElementAttribute("coordinates")]
		public string coordinates;

		public LineString ()
		{
		}
	}
}


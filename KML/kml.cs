﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    [System.Xml.Serialization.XmlRootAttribute("kml", Namespace="http://www.opengis.net/kml/2.2", IsNullable=false)]
    //	[System.Xml.Serialization.XmlArrayItemAttributes("gx", Namespace="http://www.google.com/kml/ext/2.2", IsNullable=false)]
    //[System.Xml.Serialization.XmlRoot("gx", Namespace = "http://www.google.com/kml/ext/2.2")]
    public class kml
    {
        [System.Xml.Serialization.XmlElementAttribute("Document")]
        public Document[] Document;

    }
}

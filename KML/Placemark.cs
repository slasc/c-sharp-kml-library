﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    public class Placemark
    {
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "ID")]
        public string id;

        [System.Xml.Serialization.XmlAttributeAttribute(AttributeName = "targetId")]
        public string targetId;

        [System.Xml.Serialization.XmlElementAttribute("name")]
        public string name = string.Empty;

        [System.Xml.Serialization.XmlElementAttribute("description")]
        public CDATA description = new CDATA();

        [System.Xml.Serialization.XmlElementAttribute("Point")]
        public Point Point = new Point();

		[System.Xml.Serialization.XmlElementAttribute("styleUrl")]
		public string styleUrl = string.Empty;

		[System.Xml.Serialization.XmlElementAttribute("LineString")]
		public LineString LineString;

        [System.Xml.Serialization.XmlElementAttribute("balloonVisibility", Namespace = "http://www.google.com/kml/ext/2.2")]
        public string balloonVisibility;

        public void SetPlacemark(string name, string description, double latitude, double longitude)
        {
            this.name = name;
            this.description = new CDATA(description);
            this.Point = new Point();
            this.Point.SetPoint(latitude, longitude);
        }

    }
}

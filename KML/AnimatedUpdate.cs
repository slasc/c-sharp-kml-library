﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KML
{
    public class AnimatedUpdate : Playlist
    {
        [System.Xml.Serialization.XmlElementAttribute("duration")]
        public double duration = 0.0;

        [System.Xml.Serialization.XmlElementAttribute("Update", Namespace = "http://www.opengis.net/kml/2.2")]
        public Update Update;

    }
}

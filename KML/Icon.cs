﻿using System;

namespace KML
{
	public class Icon
	{
		[System.Xml.Serialization.XmlElementAttribute("href")]
		public string href;

		[System.Xml.Serialization.XmlElementAttribute("refreshMode")]
		public string refreshMode;

		public Icon ()
		{
		}
	}
}


﻿using System;

namespace KML
{
	public class LineStyle
	{
		[System.Xml.Serialization.XmlElementAttribute("color")]
		public string color;

		[System.Xml.Serialization.XmlElementAttribute("width")]
		public string width;

		public LineStyle ()
		{
		}
	}
}


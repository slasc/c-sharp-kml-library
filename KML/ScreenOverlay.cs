﻿using System;

namespace KML
{
    public class ScreenOverlay
    {
        [System.Xml.Serialization.XmlAttributeAttribute(DataType="ID")]
        public string id;

        [System.Xml.Serialization.XmlElementAttribute("name")]
        public string name;

        [System.Xml.Serialization.XmlElementAttribute("desc")]
        public string desc;

        [System.Xml.Serialization.XmlElementAttribute("screenXY")]
        public screenXY screenXY;

        [System.Xml.Serialization.XmlElementAttribute("overlayXY")]
        public overlayXY overlayXY;

        [System.Xml.Serialization.XmlElementAttribute("rotation")]
        public double rotation;

        [System.Xml.Serialization.XmlElementAttribute("size")]
        public size size;

        [System.Xml.Serialization.XmlElementAttribute("Icon")]
        public Icon Icon;

        public ScreenOverlay()
        {
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KML
{
    public class Serialize
    {
        static XmlSerializer kmlSerializer = initkmlSerializer();

        static XmlSerializer initkmlSerializer()
        {
            XmlSerializer t = new XmlSerializer(typeof(kml));

            //t.UnknownAttribute += new XmlAttributeEventHandler(test_UnknownAttribute);
            //t.UnknownElement += new XmlElementEventHandler(test_UnknownElement);
            //t.UnknownNode += new XmlNodeEventHandler(test_UnknownNode);

            return t;
        }

        public static string GenerateXML(kml doc)
        {
            string Result = string.Empty;
            try
            {
                //System.IO.MemoryStream mem = new MemoryStream();
                //System.Xml.XmlTextWriter tw = new System.Xml.XmlTextWriter(mem, System.Text.Encoding.UTF8);
                //kmlSerializer.Serialize(tw, doc);
                //mem.Seek(0, System.IO.SeekOrigin.Begin);

                //StreamReader sr = new StreamReader(mem, System.Text.Encoding.UTF8);
                //string result = sr.ReadToEnd();
                StringWriter sw = new StringWriter();
                kmlSerializer.Serialize(sw, doc);
                string Content = sw.ToString();



                Result += Content;
            }
            catch (Exception err)
            {
                Result += err.Message + "\r\n" + err.StackTrace + "\r\n--------------\r\n";
                if (err.InnerException != null)
                    Result += err.InnerException.Message + "\r\n" + err.InnerException.StackTrace + "\r\n--------------\r\n";

            }

            return Result;

        }

        public static void WriteStringToFile(string FullPath, string Content)
        {
			File.WriteAllText(FullPath, Content);
        }

        public static string ReadStringFromFile(string FullPath)
        {
            return File.ReadAllText(FullPath);
        }

        public static kml GetDocFromXml(string xmlstring)
        {
            if (xmlstring == null)
                return null;
            if (xmlstring.Trim().Length == 0)
                return null;
            try
            {
                //				XmlRootAttribute xRoot = new XmlRootAttribute("kml");
                //				//xRoot.Namespace = "http://www.opengis.net/kml/2.2";
                //				xRoot.IsNullable = true;


                kml temp = (kml)kmlSerializer.Deserialize(new StringReader(xmlstring));

                //System.IO.StringReader Rd = new System.IO.StringReader(xmlstring);

                //System.Xml.XmlTextReader tw = new System.Xml.XmlTextReader(Rd);
                //kml temp = (kml)kmlSerializer.Deserialize(tw);
                //tw.Close();
                return temp;
            }
            catch (System.Xml.XmlException e)
            {
                string Message = string.Format("There was a System.Xml.XmlException.  The Exception says:\n{0}\n\nStack Trace is: {1}\n\nSource Data was: {2}", e.ToString(), e.StackTrace, xmlstring);
                //CommonLib.StandAloneDefines.WritelnToLogFile(Message);
                //Console.WriteLine(Message);
                throw (new Exception("There was an exception thrown.  For more information, please see the logfile.", e));
            }
            catch (System.InvalidOperationException e)
            {
                string Message = string.Format("There was a System.InvalidOperationException.  The Exception says:\n{0}\n\nStack Trace is: {1}\n\nSource Data was: {2}", e.ToString(), e.StackTrace, xmlstring);
                //CommonLib.StandAloneDefines.WritelnToLogFile(Message);
                //Console.WriteLine(Message);
                throw (new Exception("There was an exception thrown.  For more information, please see the logfile.", e));
            }

        }

        //private static void test_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        //{
        //    string Message = string.Format("Unknown Attribute error during deserialization!\r\nAttribute Name: {0}\r\nin {1}\r\n at Line number: {2} and position: {3}", e.Attr.Name, e.Attr.OuterXml, e.LineNumber, e.LinePosition);
        //    //CommonLib.StandAloneDefines.WritelnToLogFile(Message);
        //    Console.WriteLine(Message);

        //}

        //private static void test_UnknownElement(object sender, XmlElementEventArgs e)
        //{
        //    string Message = string.Format("Unknown Element error during deserialization!\r\nElement Name: {0}\r\nin {1}\r\n at Line number: {2} and position: {3}", e.Element.Name, e.Element.OuterXml, e.LineNumber, e.LinePosition);
        //    //CommonLib.StandAloneDefines.WritelnToLogFile(Message);
        //    Console.WriteLine(Message);


        //}

        //private static void test_UnknownNode(object sender, XmlNodeEventArgs e)
        //{
        //    string Message = string.Format("Unknown Node error during deserialization!\r\nNode Name: {0}\r\nwith text: {1}\r\n at Line number: {2} and position: {3}", e.Name, e.Text, e.LineNumber, e.LinePosition);
        //    //CommonLib.StandAloneDefines.WritelnToLogFile(Message);
        //    Console.WriteLine(Message);


        //}
    }
}

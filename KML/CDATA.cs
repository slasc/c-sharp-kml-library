﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;

namespace KML
{
    /// <summary>
    /// Summary description for CDATA.
    /// </summary>
    public class CDATA : IXmlSerializable

    {

        private string text;

        public CDATA()

        { }

        public CDATA(string text)

        {

            this.text = text;

        }

        public string Text

        {

            get { return text; }

        }

        XmlSchema IXmlSerializable.GetSchema()

        {

            return null;

        }

        void IXmlSerializable.ReadXml(XmlReader reader)

        {

            this.text = reader.ReadContentAsString();

        }

        void IXmlSerializable.WriteXml(XmlWriter writer)

        {

            writer.WriteCData(this.text);

        }

    }
}

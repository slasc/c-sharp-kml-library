﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KML
{
    public class Document
    {
		[System.Xml.Serialization.XmlElementAttribute("name")]
		public string name = string.Empty;

		[System.Xml.Serialization.XmlElementAttribute("description")]
		public string description = string.Empty;

		[System.Xml.Serialization.XmlElementAttribute("Placemark")]
        public Placemark[] Placemark;

		[System.Xml.Serialization.XmlElementAttribute("Style")]
		public Style[] Style;

        [System.Xml.Serialization.XmlElementAttribute("ScreenOverlay")]
        public ScreenOverlay[] ScreenOverlay;

        [System.Xml.Serialization.XmlElementAttribute( "Tour", Namespace = "http://www.google.com/kml/ext/2.2")]
        public Tour Tour;

    }
}
